# BINARY SEMAPHORE EXAMPLE #

Semplice esempio di utilizzo della classe `java.util.concurrent.Semaphore`.
Due thread concorrenti utilizzano un semaforo binario per regolare l'accesso alla sezione critica.